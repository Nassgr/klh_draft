# klh_draft

Process for KLH recruitment : 

**Steps**

Clone the repos from GitLab.
Run composer install.

**Define in .env file : **

DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7

Create the database in sql if it is not exist.
Run the server with command : symfony server:start

Enjoy.
